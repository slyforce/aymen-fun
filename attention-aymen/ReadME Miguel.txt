Also Daten sind drin schon angepasst
- Ich bekomme den Fehler wenn ich beispielsweise folgenden Befehl ausführe:
python run.py --problem cvrp  --graph_size 10  --baseline rollout --run_name vrp10

-Zum Ausführen vom ganzen, brauchste am besten ein Conda-Env (conda create -n torch python=3.7) 
mit folgenden Abhängigkeiten:
*Python>=3.6
*NumPy 
*SciPy 
*PyTorch=0.4 (conda install pytorch torchvision cudatoolkit=10.0 -c pytorch)
*tqdm (pip install tqdm)
*tensorboard_logger (pip install tensorboard_logger )
*Matplotlib (optional, only for plotting)
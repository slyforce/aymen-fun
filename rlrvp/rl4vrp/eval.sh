#!/bin/bash

test_file="/home/miguel/src/aymen-fun/rlrvp/rl4vrp/data/vrp-size-1000-len-4-test.txt";

python main.py \
--task=vrp10 \
--beam_width 3   \
--dataset="vrp10_7200_800_2000_26102019012121.pkl" \
--lgbm_model=VRP/lgb_24102019.pkl  \
--is_train=false \
--infer_type=single \
--analyze_output="./test" 

echo "Result for test set: $test_file "

# --load_path="logs/vrp10-2019-10-13_14-30-18/model/model.ckpt-200" \


echo "Greedy:"
grep "Validation overall avg_reward" ./test.greedy.validation.txt;

echo "Beam search:"
grep "Validation overall avg_reward" ./test.beam_search.validation.txt;




#!/bin/bash

python main.py \
--task=vrp10 \
--dataset="vrp10_7200_800_2000_26102019012121.pkl" \
--n_train 123456 \
--batch_size 32 \
--beam_width 3   \
--log_interval=10 \
--save_interval=100 \
--lgbm_model=VRP/lgb_24102019.pkl;


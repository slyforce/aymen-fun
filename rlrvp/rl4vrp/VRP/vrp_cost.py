from sklearn.externals import joblib
from lightgbm import LGBMRegressor
import numpy as np
import tensorflow as tf
from pathlib import Path

from .vrp_utils import debug_tensor

class Scorer:

    def __init__(self,model_path):
        print("Opening scorer from path {}".format(model_path))
        assert Path(model_path).is_file(), "{} was not found.".format(model_path)
        self.lgb = joblib.load(model_path)#'lgb.pkl'

    def predict_scrap(self, item1, item2, work_center=1):
        features_vals = abs(item2 - item1).tolist()
        return np.exp(self.lgb.predict([[work_center] + features_vals]))

    def _replace_zeros_with_epsilon(self, X, epsilon=1e-12):
        # X: np.array
        return np.where(X != 0.0, X, np.full(shape=X.shape, fill_value=epsilon))

    def batched_predict(self, X1, X2, work_center=1):
        """
        shape X1, X2 = [num_samples, num_features]
        """
        depot_ind = np.where(~X1.any(axis=1))[0]# get depot indexes
        num_samples = X1.shape[0]

        # prepare the features
        #feature_values = np.nan_to_num(100 * X1 / self._replace_zeros_with_epsilon(X2)) # old reward
        feature_values = abs(X2-X1)
        work_center_feature = np.full(shape=(num_samples,1), fill_value=work_center)
        lgb_input_features = np.concatenate([work_center_feature, feature_values], axis=1)

        # run prediction
        # shape [num_samples]
        predictions = np.exp(self.lgb.predict(lgb_input_features))
        predictions[depot_ind] = 0. # any order that gets back to depot(zero row vector) should have 0 cost
        # cast down to float32 (previously float64)
        predictions = predictions.astype(np.float32)
        return predictions

    def batched_predict_tf(self, X1, X2, work_center=1):
        # shape X1, X2 = [num_samples, num_features]
        return tf.py_func(self.batched_predict,
                          [X1, X2, work_center],
                          [tf.float32],
                          stateful=True,
                          name="LGBMRegressorPyfunc")

    def reward_function(self, tour_solution, tour_lengths, machine_idxs=None):
        """
        Computes the reward function using the lgbm regressor.
        This is done by taking in the tours are comparing adjacent sequence timesteps.
        E.g. first step is compared with second step, and so on....
        The reward function takes into account the return to the depot!!

        A high reward is BAD.

        TODO: Include machine idxs in the loss function (possibly with some kind of weighting)
        :param tour_solution: List([batch_size, feature_dim])
        :param tour_lengths: [batch_size] how long the tour is (for paddings)
        :param machine_idxs:  [batch_size] or None
                               corresponds to the final machine index reached in the tour
                               should be punished if gone over the number of possible machines
                               (soft constaint on the number of machines)
        """

        with tf.name_scope("RewardFunctionLGBM"):
            # [num_timesteps, batch_size, feature_dim]
            tour_solution = tf.stack(tour_solution, axis=0)

            num_timesteps = tf.shape(tour_solution)[0]
            batch_size = tf.shape(tour_solution)[1]
            feature_dim = tf.shape(tour_solution)[-1]

            source = tour_solution[1:, :, :]
            destination = tour_solution[:-1, :, :]

            # flatten tensors for batched eval
            source = tf.reshape(source, shape=[-1, feature_dim])
            destination = tf.reshape(destination, shape=[-1, feature_dim])

            # run prediction
            # shape [(num_timesteps-1) * batch_size]
            prediction = self.batched_predict_tf(source, destination, work_center=1)

            # reshape back to [num_timesteps-1, batch_size]
            prediction = tf.reshape(prediction, shape=[-1, batch_size])

            # take into account the length of the tour to compute the reward function
            # if you stayed in the depot at the end for several positions then don't take that into account
            # padding_mask shape: [batch_size, num_timesteps-1]
            padding_mask = tf.sequence_mask(tour_lengths, num_timesteps-1) # -1 because we shifted the sequences

            # padding_mask shape: [num_timesteps-1, batch_size]
            padding_mask = tf.transpose(padding_mask, perm=[1,0])

            # prediction shape: [num_timesteps-1, batch_size]
            prediction = tf.multiply(tf.cast(padding_mask, tf.float32), prediction)

            # sum over timesteps
            # shape [batch_size]
            reward_per_instance_per_position = prediction
            reward_per_instance = tf.reduce_sum(prediction, axis=0)

            if machine_idxs is not None:
                with tf.name_scope("MachineCosts"):
                    # example code of adding costs
                    num_machines = 9 # todo: change me
                    cost_for_using_more_machines = 1000.0 # todo: change me
                    cost = tf.where(tf.greater_equal(machine_idxs, num_machines),
                                    tf.fill(dims=[batch_size], value=cost_for_using_more_machines),
                                    tf.zeros_like(reward_per_instance))

                    reward_per_instance += cost

        return reward_per_instance, reward_per_instance_per_position

if __name__ == '__main__':
    scr=Scorer('lgb_24102019.pkl')
    item1 = np.array([[0.96113634, 600.0, 0.008, 400.0, 15.378181, 173.0, 216.0, 230.0, 1.0]])
    item2 = np.array([[1.0118713, 800.0, 0.041, 0.0, 66.37876, 173.0, 216.0, 230.0, 5.0]])
    print(scr.batched_predict(item1,item2,1))

    print("Giving the same input in: ")
    print(scr.batched_predict(item1, item1, 1))
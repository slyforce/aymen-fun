from misc_utils import debug_tensor

import numpy as np
import tensorflow as tf
import os
import warnings
import collections
####################
import pandas as pd
import random
####################
class mkData():

    def __init__(self, rnd, load_from_file="cleaned_items.pkl"):
        self.items_df = pd.read_pickle('Pairs_Gurobi.pkl', compression='gzip')
        self.items = list(set(self.items_df['Item No_ 1']))
        self.rnd=rnd
        self.features = ['Dichte 1', 'Breite 1', 'Dicke 1', 'Seitenfalte 1', 'EchtGew1000M 1',
                    'RED 1', 'GREEN 1', 'BLUE 1','OPACITY 1']

    def get_items_data(self, items_list):
        items_df = self.items_df
        if len(set(items_list).difference(set(items_df['Item No_ 1']))) == 0:
            return items_df[self.features].loc[items_df['Item No_ 1'].isin(items_list)].values
        else:
            print('{} items list is violated'.format(items_list))

    def generate_dataset(self,n_problems, n_nodes):
        return np.asarray([self.get_items_data(self.rnd.choice(self.items, replace=False, size=(n_nodes,))) for _ in
                           range(n_problems)])

    def load_dataset(self):
        """
        Loads all the examples from a file in contrast to generate_dataset, which *samples* instances from file.
        """
        return np.array(self.items_df[self.features].values)


def create_VRP_dataset(
        n_problems,
        n_cust,
        data_dir,
        seed=None,
        data_type='train',
        explicit_test_load=""):
    '''
    This function creates VRP instances and saves them on disk. If a file is already available,
    it will load the file.
    Input:
        n_problems: number of problems to generate.
        n_cust: number of customers in the problem.
        data_dir: the directory to save or load the file.
        seed: random seed for generating the data.
        data_type: the purpose for generating the data. It can be 'train', 'val', or any string.
    output:
        data: a numpy array with shape [n_problems x (n_cust+1) x 3]
        in the last dimension, we have x,y,demand for customers. The last node is for depot and 
        it has demand 0.
     '''

    # set random number generator
    n_nodes = n_cust +1
    dimension = 9
    if seed == None:
        rnd = np.random
    else:
        rnd = np.random.RandomState(seed)
    
    # build task name and datafiles
    task_name = 'vrp-size-{}-len-{}-{}.txt'.format(n_problems, n_nodes,data_type)
    fname = os.path.join(data_dir, task_name)
    if explicit_test_load != "":
        fname = explicit_test_load
        from pathlib import Path
        assert Path(fname).is_file(), "File {} does not exist!".format(fname)

    # cteate/load data
    if os.path.exists(fname):
        print('Loading dataset for {}...'.format(task_name))
        data = np.loadtxt(fname,delimiter=' ')
        data = data.reshape(-1, n_nodes, dimension + 1)
    else:
        #####################################################################################################
        items_df = pd.read_pickle('Pairs_Gurobi.pkl', compression='gzip')
        items = list(set(items_df['Item No_ 1']))

        print('Creating dataset for {}...'.format(task_name))
        mk = mkData(rnd)
        x = mk.generate_dataset(n_problems, n_nodes)
        print(x.shape)
        d = np.ones((n_problems,n_nodes,1),dtype=int)
        print(d.shape)
        d[:, -1] = 0
        data = np.concatenate([x, d], 2)
        #######################################################################################################
        # Generate a training set of size n_problems 
        #x = rnd.uniform(0,1,size=(n_problems,n_nodes,2))
        #d = rnd.randint(1,10,[n_problems,n_nodes,1])
        #d[:,-1]=0 # demand of depot
        #data = np.concatenate([x,d],2)
        np.savetxt(fname, data.reshape(-1, n_nodes*(dimension+1)))

    return data


def create_data_from_pandas_dataframes(n_cust, train_data, test_data, seed=None):
    # set random number generator
    n_nodes = n_cust + 1
    if seed == None:
        rnd = np.random
    else:
        rnd = np.random.RandomState(seed)

    def prepare_dataset(features):
        n_problems = features.shape[0]
        d = np.ones((n_problems, n_nodes, 1), dtype=int)
        d[:, -1, :] = 0 # demand for depot is always 0
        # shape: [num_examples, num_nodes, 10] (feature_dim + demand)
        return np.concatenate([features, d], 2)

    # generate the training dataset randomly
    mk_train_data = mkData(rnd, train_data)
    #train_data = prepare_dataset(mk_train_data.load_dataset())
    train_data = prepare_dataset(mk_train_data.generate_dataset(1000, n_nodes))# TODO remove this. this is just for testing

    # generate the test data by taking the whole dataset
    mk_test_data = mkData(rnd, test_data)
    #test_data = prepare_dataset(mk_test_data.load_dataset())
    test_data = prepare_dataset(mk_test_data.generate_dataset(100, n_nodes)) # TODO remove this. this is just for testing

    return train_data, test_data


class DatasetIterator(object):
    def __init__(self, X, batch_size, shuffle=False,
                 gurobi_tours=None, gurobi_scores=None):
        """

        :param X: np.ndarray shape: [num_samples, num_nodes, num_features]
        :param y:
        """
        self.num_samples = X.shape[0]
        self.num_nodes = X.shape[1]
        self.num_features = X.shape[2]
        self.index = 0
        self.epoch_counter = -1 # gets incremented to 0 after reset()
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.X = X
        self.gurobi_tours = gurobi_tours
        self.gurobi_scores = gurobi_scores

        self.reset()

    def reset(self):
        self.index = 0
        self.epoch_counter += 1
        if self.shuffle:
            assert self.gurobi_scores is None and self.gurobi_tours is None, "Do not shuffle with gurobi results"
            indices = np.arange(self.num_samples)
            np.random.shuffle(indices)
            self.X = self.X[indices]

    def get_next(self, with_y=False):
        tour, score = None, None
        if self.index + self.batch_size > self.num_samples:
            try:
                if with_y:
                    assert False, "You should not end up in this case when gurobi information is requested"

                # batch size for this batch would be too small
                # peek into the next iteration
                num_samples_in_firsthalf = self.num_samples - self.index
                num_samples_in_secondhalf = self.batch_size - num_samples_in_firsthalf

                X1 = self.X[self.index:]

                # now reset to get a new order
                self.reset()

                X2 = self.X[:num_samples_in_secondhalf]
                X = np.concatenate([X1, X2], axis=0)

                self.index = num_samples_in_secondhalf
            except Exception as e:
                print("This code block has not been tested. "
                      "Please check it for errors.")
                raise Exception(e)
        else:
            X = self.X[self.index: self.index+self.batch_size, :, :]
            if with_y:
                assert self.batch_size == 1, "Batch size 1 required for gurobi information"
                tour = self.gurobi_tours[self.index]
                score = self.gurobi_scores[self.index]

            self.index += self.batch_size
            if self.index == self.num_samples:
                self.reset()

        return X, (tour, score)

class NewDataGenerator(object):
    def __init__(self,
                 args):

        '''
        This class generates VRP problems for training and test
        Inputs:
            args: the parameter dictionary. It should include:
                args['random_seed']: random seed
                args['test_size']: number of problems to test
                args['n_nodes']: number of nodes
                args['n_cust']: number of customers
                args['batch_size']: batchsize for training

        '''
        self.args = args
        self.rnd = np.random.RandomState(seed=args['random_seed'])
        print('Created train iterator.')
        self.read_dataset_pickle()
        self.reset()

    def read_dataset_pickle(self):
        from pathlib import Path
        import pickle as pkl
        filepath = Path(self.args["dataset"])
        assert filepath.exists(), "File {} does not exist".format(filepath)

        with open(filepath, "rb") as f:
            data_dict = pkl.load(f)
            for k, v in data_dict.items():
                print("Shape of {}: {} ".format(k, v.shape))

            self.train_iterator = DatasetIterator(data_dict["X_train"], self.args["batch_size"], shuffle=True)

            self.test_iterator = DatasetIterator(data_dict["X_val"], batch_size=1, shuffle=False,
                                                  gurobi_tours=data_dict["y_val"][:,:-1],
                                                  gurobi_scores=data_dict["y_val"][:,-1])
            self.eval_iterator = DatasetIterator(data_dict["X_test"], batch_size=1, shuffle=False,
                                                  gurobi_tours=data_dict["y_test"][:,:-1],
                                                  gurobi_scores=data_dict["y_test"][:,-1])

    def reset(self):
        self.count = 0

    def get_train_next(self):
        '''
        Get next batch of problems for training
        Retuens:
            input_data: data with shape [batch_size x max_time x 3]
        '''

        # sample batch size many examples from training data
        idxs = self.rnd.random_integers(low=0, high=self.train_data.shape[0]-1, size=(self.args["batch_size"], ))

        # shape: [batch_size, num_nodes, feature_dim+1]
        return self.train_data[idxs, :, :]

    def get_test_next(self):
        '''
        Get next batch of problems for testing
        '''
        if self.count < self.args['test_size']:
            input_pnt = self.test_data[self.count:self.count + 1]
            self.count += 1
        else:
            warnings.warn("The test iterator reset.")
            self.count = 0
            input_pnt = self.test_data[self.count:self.count + 1]
            self.count += 1

        return input_pnt

    def get_test_all(self):
        '''
        Get all test problems
        '''
        return self.test_data

    def get_next_train_batch(self):
        yield self.train_iterator.get_next(with_y=False)

    def get_eval_samples(self):
        self.eval_iterator.reset()
        self.eval_iterator.epoch_counter = 0
        while self.eval_iterator.epoch_counter != 1:
            yield self.eval_iterator.get_next(with_y=True) # todo: change to true

    def get_test_samples(self):
        self.test_iterator.reset()
        self.test_iterator.epoch_counter = 0
        while self.test_iterator.epoch_counter != 1:
            yield self.test_iterator.get_next(with_y=True) # todo: change to true


class DataGenerator(object):
    def __init__(self, 
                 args):

        '''
        This class generates VRP problems for training and test
        Inputs:
            args: the parameter dictionary. It should include:
                args['random_seed']: random seed
                args['test_size']: number of problems to test
                args['n_nodes']: number of nodes
                args['n_cust']: number of customers
                args['batch_size']: batchsize for training

        '''
        self.args = args
        self.rnd = np.random.RandomState(seed= args['random_seed'])
        print('Created train iterator.')

        # create test data
        self.mk = mkData(self.rnd)
        self.n_problems = args['test_size']
        self.test_data = create_VRP_dataset(self.n_problems,args['n_cust'],'./data',
            seed = args['random_seed']+1,data_type='test', explicit_test_load=args["test_file"])

        self.reset()

    def reset(self):
        self.count = 0

    def get_train_next(self):
        '''
        Get next batch of problems for training
        Retuens:
            input_data: data with shape [batch_size x max_time x 3]
        '''

        #input_pnt = self.rnd.uniform(0,1,  size=(self.args['batch_size'],self.args['n_nodes'], 2))
        #input_pnt = self.rnd.uniform(0,1,  size=(self.args['batch_size'],self.args['n_nodes'], 9))
        input_pnt = self.mk.generate_dataset(self.args['batch_size'],self.args['n_nodes'])

        #demand = self.rnd.randint(1,10,[self.args['batch_size'],self.args['n_nodes']])
        demand = np.ones([self.args['batch_size'],self.args['n_nodes']],dtype=int)
        demand[:,-1]=0 # demand of depot

        input_data = np.concatenate([input_pnt,np.expand_dims(demand,2)],2)
        return input_data

 
    def get_test_next(self):
        '''
        Get next batch of problems for testing
        '''
        if self.count<self.args['test_size']:
            input_pnt = self.test_data[self.count:self.count+1]
            self.count +=1
        else:
            warnings.warn("The test iterator reset.") 
            self.count = 0
            input_pnt = self.test_data[self.count:self.count+1]
            self.count +=1

        return input_pnt

    def get_test_all(self):
        '''
        Get all test problems
        '''
        return self.test_data
    

class State(collections.namedtuple("State",
                                        ("load",
                                         "demand",
                                         'd_sat',
                                         "mask",
                                         "cur_machine",
                                         "tour_length"))):
    pass
    
class Env(object):
    def __init__(self,
                 args):
        '''
        This is the environment for VRP.
        Inputs: 
            args: the parameter dictionary. It should include:
                args['n_nodes']: number of nodes in VRP
                args['n_custs']: number of customers in VRP
                args['input_dim']: dimension of the problem which is 2
        '''
        self.capacity = args['capacity']
        self.n_nodes = args['n_nodes']
        self.n_cust = args['n_cust']
        self.input_dim = args['input_dim']
        self.input_data = tf.placeholder(tf.float32,\
            shape=[None,self.n_nodes,self.input_dim])

        self.input_pnt = self.input_data[:,:,:-1]
        self.demand = self.input_data[:,:,-1]
        self.batch_size = tf.shape(self.input_pnt)[0]

        self.node_width  =self.input_data[:,:,1]
        self.width_for_machines = tf.constant([500, 600, 800, 1000, 1200, 1200, 1200, 1200, 1200],
                                              dtype=tf.float32) # shape [num_machines]
        self.num_machines = 9

        # shape: [batch_size, num_machines, num_nodes]
        machine_width = tf.reshape(self.width_for_machines, shape=[1, -1, 1])
        machine_width = tf.tile(machine_width, multiples=[self.batch_size, 1, self.n_nodes])
        self.width_mask = tf.cast(tf.reshape(self.node_width, [self.batch_size, 1, self.n_nodes]) > machine_width, tf.int32)

        # reshape width mask for repeats across beam size
        # shape: [batch_size, 1, num_machines, num_nodes]
        self.width_mask_for_repeats = tf.expand_dims(self.width_mask, axis=1)
        print("width_mask_for_repeats shape", self.width_mask_for_repeats.shape)

    def reset(self,beam_width=1):
        '''
        Resets the environment. This environment might be used with different decoders.
        In case of using with beam-search decoder, we need to have to increase
        the rows of the mask by a factor of beam_width.
        '''

        # dimensions
        self.beam_width = beam_width
        self.batch_beam = self.batch_size * beam_width

        self.input_pnt = self.input_data[:,:,:-1]
        self.demand = self.input_data[:,:,-1]

        # modify the self.input_pnt and self.demand for beam search decoder
#         self.input_pnt = tf.tile(self.input_pnt, [self.beam_width,1,1])
        # demand: [batch_size * beam_width, max_time]
        # demand[i] = demand[i+batchsize]
        self.demand = tf.tile(self.demand, [self.beam_width,1])

        # load: [batch_size * beam_width]
        self.load = tf.ones([self.batch_beam])*self.capacity

        # create mask
        self.mask = tf.zeros([self.batch_size*beam_width,self.n_nodes],
                dtype=tf.float32)

        # update mask -- mask if customer demand is 0 and depot
        self.mask = tf.concat([tf.cast(tf.equal(self.demand,0), tf.float32)[:,:-1],
            tf.ones([self.batch_beam,1])],1)

        self.cur_machine = tf.zeros([self.batch_beam], dtype=tf.int32)
        self.width_mask = tf.tile(self.width_mask_for_repeats, multiples=[1,beam_width,1,1])
        self.width_mask = tf.reshape(self.width_mask, shape=[self.batch_beam, self.num_machines, self.n_nodes])

        self.tour_length = tf.ones([self.batch_beam], dtype=tf.int32) # previously 0, now 1 because you always start at depot (only for the reward function)

        state = State(load=self.load,
                      demand = self.demand,
                      d_sat = tf.zeros([self.batch_beam,self.n_nodes]),
                      mask = self.mask,
                      cur_machine=self.cur_machine,
                      tour_length=self.tour_length)

        return state

    def step(self,
             idx,
             beam_parent=None):
        '''
        runs one step of the environment and updates demands, loads and masks
        '''

        # if the environment is used in beam search decoder
        if beam_parent is not None:
            # BatchBeamSeq: [batch_size*beam_width x 1]
            # [0,1,2,3,...,127,0,1,...],
            batchBeamSeq = tf.expand_dims(tf.tile(tf.cast(tf.range(self.batch_size), tf.int64),
                                                 [self.beam_width]),1)
            # batchedBeamIdx:[batch_size*beam_width]
            batchedBeamIdx= batchBeamSeq + tf.cast(self.batch_size,tf.int64)*beam_parent
            # demand:[batch_size*beam_width x sourceL]
            self.demand= tf.gather_nd(self.demand,batchedBeamIdx)
            #load:[batch_size*beam_width]
            self.load = tf.gather_nd(self.load,batchedBeamIdx)
            #MASK:[batch_size*beam_width x sourceL]
            self.mask = tf.gather_nd(self.mask,batchedBeamIdx)

            self.cur_machine = tf.gather_nd(self.cur_machine, batchedBeamIdx)
            self.tour_length = tf.gather_nd(self.tour_length, batchedBeamIdx)


        BatchSequence = tf.expand_dims(tf.cast(tf.range(self.batch_beam), tf.int64), 1)
        batched_idx = tf.concat([BatchSequence,idx],1)

        # how much the demand is satisfied
        d_sat = tf.minimum(tf.gather_nd(self.demand,batched_idx), self.load)

        # update the demand
        d_scatter = tf.scatter_nd(batched_idx, d_sat, tf.cast(tf.shape(self.demand),tf.int64))
        self.demand = tf.subtract(self.demand, d_scatter)

        # update load
        self.load -= d_sat

        # refill the truck -- idx: [10,9,10] -> load_flag: [1 0 1]
        load_flag_bool = tf.equal(idx,self.n_cust)
        load_flag = tf.squeeze(tf.cast(load_flag_bool,tf.float32),1)
        self.load = tf.multiply(self.load,1-load_flag) + load_flag *self.capacity

        # mask for customers with zero demand
        self.mask = tf.concat([tf.cast(tf.equal(self.demand,0), tf.float32)[:,:-1],
                                          tf.zeros([self.batch_beam,1])],1)

        # mask if load= 0 
        # mask if in depot and there is still a demand

        self.mask += tf.concat( [tf.tile(tf.expand_dims(tf.cast(tf.equal(self.load,0),
            tf.float32),1), [1,self.n_cust]),                      
            tf.expand_dims(tf.multiply(tf.cast(tf.greater(tf.reduce_sum(self.demand,1),0),tf.float32),
                             tf.squeeze( tf.cast(tf.equal(idx,self.n_cust),tf.float32))),1)],1)

        # shape [batch_size]
        no_demand_in_customers = tf.equal(tf.reduce_sum(self.demand, axis=1), 0.0)
        is_in_depot_and_no_demand_in_customers = tf.logical_and(tf.squeeze(load_flag_bool),
                                                                no_demand_in_customers)
        self.tour_length += tf.where(is_in_depot_and_no_demand_in_customers,
                                     tf.zeros_like(self.tour_length),
                                     tf.ones_like(self.tour_length))

        if True:
            # update current machine index
            # this is done whenever there is demand from customers and you returned to the depot

            demand_in_customers_and_in_depot = tf.logical_and(tf.logical_not(no_demand_in_customers),
                                                              tf.squeeze(load_flag_bool))
            self.cur_machine += tf.where(demand_in_customers_and_in_depot,
                                         tf.ones_like(self.cur_machine),
                                         tf.zeros_like(self.cur_machine))

            # self.cur_machine = debug_tensor(self.cur_machine, "before")
            # self.cur_machine = tf.minimum(self.cur_machine, 8)  # never reach a machine idx of 9, since there are only 8 indices!!
            # self.cur_machine = debug_tensor(self.cur_machine, "after")

            # Attention! This is just for width constraints.
            # This is *not* needed for standard training.

            # apply a mask for the machine
            # gathered_indices = tf.gather_nd(self.width_mask,
            #                                 tf.stack([tf.range(self.batch_beam), self.cur_machine], axis=1))
            # s elf.mask += tf.cast(gathered_indices, tf.float32)


        state = State(load=self.load,
                      demand = self.demand,
                      d_sat = d_sat,
                      mask = self.mask ,
                      cur_machine=self.cur_machine,
                      tour_length=self.tour_length)

        return state

def reward_func(sample_solution, tour_lengths=None, machine_indices=None):
    """The reward for the VRP task is defined as the 
    negative value of the route length

    Args:
        sample_solution : a list tensor of size decode_len of shape [batch_size x input_dim]
        demands satisfied: a list tensor of size decode_len of shape [batch_size]

    Returns:
        rewards: tensor of size [batch_size]

    Example:
        sample_solution = [[[1,1],[2,2]],[[3,3],[4,4]],[[5,5],[6,6]]]
        sourceL = 3
        batch_size = 2
        input_dim = 2
        sample_solution_tilted[ [[5,5]
                                                    #  [6,6]]
                                                    # [[1,1]
                                                    #  [2,2]]
                                                    # [[3,3]
                                                    #  [4,4]] ]
    """
    # make init_solution of shape [sourceL x batch_size x input_dim]

    # make sample_solution of shape [sourceL x batch_size x input_dim]
    sample_solution = tf.stack(sample_solution,0)

    sample_solution_tilted = tf.concat((tf.expand_dims(sample_solution[-1],0),
         sample_solution[:-1]),0)
    # get the reward based on the route lengths


    route_lens_decoded = tf.reduce_sum(tf.pow(tf.reduce_sum(tf.pow(\
        (sample_solution_tilted - sample_solution) ,2), 2) , .5), 0)
    return route_lens_decoded 


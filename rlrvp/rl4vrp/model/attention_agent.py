import tensorflow as tf
import numpy as np
import time
from shared.embeddings import LinearEmbedding
from shared.decode_step import RNNDecodeStep

class RLAgent(object):
    
    def __init__(self,
                args,
                prt,
                env,
                dataGen,
                reward_func,
                clAttentionActor,
                clAttentionCritic,
                is_train=True,
                _scope=''):
        '''
        This class builds the model and run testt and train.
        Inputs:
            args: arguments. See the description in config.py file.
            prt: print controller which writes logs to a file.
            env: an instance of the environment.
            dataGen: a data generator which generates data for test and training.
            reward_func: the function which is used for computing the reward. In the 
                        case of TSP and VRP, it returns the tour length.
            clAttentionActor: Attention mechanism that is used in actor.
            clAttentionCritic: Attention mechanism that is used in critic.
            is_train: if true, the agent is used for training; else, it is used only 
                        for inference.
        '''
        
        self.args = args
        self.prt = prt
        self.env = env
        self.dataGen = dataGen
        self.reward_func = reward_func
        self.clAttentionCritic = clAttentionCritic
        
        self.embedding = LinearEmbedding(args['embedding_dim'],
            _scope=_scope+'Actor/')
        self.decodeStep = RNNDecodeStep(clAttentionActor,
                        args['hidden_dim'], 
                        use_tanh=args['use_tanh'],
                        tanh_exploration=args['tanh_exploration'],
                        n_glimpses=args['n_glimpses'],
                        mask_glimpses=args['mask_glimpses'], 
                        mask_pointer=args['mask_pointer'], 
                        forget_bias=args['forget_bias'], 
                        rnn_layers=args['rnn_layers'],
                        _scope='Actor/')
        self.decoder_input = tf.get_variable('decoder_input', [1,1,args['embedding_dim']],
                       initializer=tf.contrib.layers.xavier_initializer())

        start_time  = time.time()
        if is_train:
            self.train_summary = self.build_model(decode_type = "stochastic" )
            self.train_step = self.build_train_step()

        self.val_summary_greedy = self.build_model(decode_type = "greedy" )
        self.val_summary_beam = self.build_model(decode_type = "beam_search")

        model_time = time.time()- start_time
        self.prt.print_out("It took {}s to build the agent.".format(str(model_time)))

        self.saver = tf.train.Saver(
            var_list=tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))
            
        
    def build_model(self, decode_type = "greedy"):
        
        # builds the model
        args = self.args
        env = self.env
        batch_size = tf.shape(env.input_pnt)[0]

        # input_pnt: [batch_size x max_time x 2]
        input_pnt = env.input_pnt
        # encoder_emb_inp: [batch_size, max_time, embedding_dim]
        encoder_emb_inp = self.embedding(input_pnt)

        if decode_type == 'greedy' or decode_type == 'stochastic':
            beam_width = 1
        elif decode_type == 'beam_search': 
            beam_width = args['beam_width']
            
        # reset the env. The environment is modified to handle beam_search decoding.
        env.reset(beam_width)

        BatchSequence = tf.expand_dims(tf.cast(tf.range(batch_size*beam_width), tf.int64), 1)


        # create tensors and lists
        actions_tmp = []
        logprobs = []
        probs = []
        idxs = []

        # start from depot
        idx = (env.n_nodes-1)*tf.ones([batch_size*beam_width,1])
        action = tf.tile(input_pnt[:,env.n_nodes-1],[beam_width,1])


        # decoder_state
        initial_state = tf.zeros([args['rnn_layers'], 2, batch_size*beam_width, args['hidden_dim']])
        l = tf.unstack(initial_state, axis=0)
        decoder_state = tuple([tf.nn.rnn_cell.LSTMStateTuple(l[idx][0],l[idx][1])
                  for idx in range(args['rnn_layers'])])            

        # start from depot in VRP and from a trainable nodes in TSP
        # decoder_input: [batch_size*beam_width x 1 x hidden_dim]
        if args['task_name'] == 'tsp':
            # decoder_input: [batch_size*beam_width x 1 x hidden_dim]
            decoder_input = tf.tile(self.decoder_input, [batch_size* beam_width,1,1])
        elif args['task_name'] == 'vrp':
            decoder_input = tf.tile(tf.expand_dims(encoder_emb_inp[:,env.n_nodes-1], 1), 
                                    [beam_width,1,1])

        # decoding loop
        context = tf.tile(encoder_emb_inp,[beam_width,1,1])
        for i in range(args['decode_len']):
            
            logit, prob, logprob, decoder_state = self.decodeStep.step(decoder_input,
                                context,
                                env,
                                decoder_state)
            # idx: [batch_size*beam_width x 1]
            beam_parent = None
            if decode_type == 'greedy':
                idx = tf.expand_dims(tf.argmax(prob, 1),1)
            elif decode_type == 'stochastic':
                # select stochastic actions. idx has shape [batch_size x 1]
                # tf.multinomial sometimes gives numerical errors, so we use our multinomial :(
                def my_multinomial():
                    prob_idx = tf.stop_gradient(prob)
                    prob_idx_cum = tf.cumsum(prob_idx,1)
                    rand_uni = tf.tile(tf.random_uniform([batch_size,1]),[1,env.n_nodes])
                    # sorted_ind : [[0,1,2,3..],[0,1,2,3..] , ]
                    sorted_ind = tf.cast(tf.tile(tf.expand_dims(tf.range(env.n_nodes),0),[batch_size,1]),tf.int64)
                    tmp = tf.multiply(tf.cast(tf.greater(prob_idx_cum,rand_uni),tf.int64), sorted_ind)+\
                        10000*tf.cast(tf.greater_equal(rand_uni,prob_idx_cum),tf.int64)

                    idx = tf.expand_dims(tf.argmin(tmp,1),1)
                    return tmp, idx

                tmp, idx = my_multinomial()
                # check validity of tmp -> True or False -- True mean take a new sample
                tmp_check = tf.cast(tf.reduce_sum(tf.cast(tf.greater(tf.reduce_sum(tmp,1),(10000*env.n_nodes)-1),
                                                          tf.int32)),tf.bool)
                tmp , idx = tf.cond(tmp_check,my_multinomial,lambda:(tmp,idx))

            elif decode_type == 'beam_search':
                if i==0:
                    # BatchBeamSeq: [batch_size*beam_width x 1]
                    # [0,1,2,3,...,127,0,1,...],
                    batchBeamSeq = tf.expand_dims(tf.tile(tf.cast(tf.range(batch_size), tf.int64),
                                                         [beam_width]),1)
                    beam_path  = []
                    log_beam_probs = []
                    # in the initial decoder step, we want to choose beam_width different branches
                    # log_beam_prob: [batch_size, sourceL]
                    log_beam_prob = tf.log(tf.split(prob,num_or_size_splits=beam_width, axis=0)[0])

                elif i > 0:
                    log_beam_prob = tf.log(prob) + log_beam_probs[-1]
                    # log_beam_prob:[batch_size, beam_width*sourceL]
                    log_beam_prob = tf.concat(tf.split(log_beam_prob, num_or_size_splits=beam_width, axis=0),1)

                # topk_prob_val,topk_logprob_ind: [batch_size, beam_width]
                topk_logprob_val, topk_logprob_ind = tf.nn.top_k(log_beam_prob, beam_width)

                # topk_logprob_val , topk_logprob_ind: [batch_size*beam_width x 1]
                topk_logprob_val = tf.transpose(tf.reshape(
                    tf.transpose(topk_logprob_val), [1,-1]))

                topk_logprob_ind = tf.transpose(tf.reshape(
                    tf.transpose(topk_logprob_ind), [1,-1]))

                #idx,beam_parent: [batch_size*beam_width x 1]                               
                idx = tf.cast(topk_logprob_ind % env.n_nodes, tf.int64) # Which city in route.
                beam_parent = tf.cast(topk_logprob_ind // env.n_nodes, tf.int64) # Which hypothesis it came from.

                # batchedBeamIdx:[batch_size*beam_width]
                batchedBeamIdx= batchBeamSeq + tf.cast(batch_size,tf.int64)*beam_parent
                prob = tf.gather_nd(prob,batchedBeamIdx)

                beam_path.append(beam_parent)
                log_beam_probs.append(topk_logprob_val)

            state = env.step(idx,beam_parent)
            batched_idx = tf.concat([BatchSequence,idx],1)


            decoder_input = tf.expand_dims(tf.gather_nd(
                tf.tile(encoder_emb_inp,[beam_width,1,1]), batched_idx),1)

            logprob = tf.log(tf.gather_nd(prob, batched_idx))
            probs.append(prob)
            idxs.append(idx)
            logprobs.append(logprob)           

            action = tf.gather_nd(tf.tile(input_pnt, [beam_width,1,1]), batched_idx )
            actions_tmp.append(action)

        final_state = state

        if decode_type=='beam_search':
            # find paths of the beam search
            tmplst = []
            tmpind = [BatchSequence]
            for k in reversed(range(len(actions_tmp))):

                tmplst = [tf.gather_nd(actions_tmp[k],tmpind[-1])] + tmplst
                tmpind += [tf.gather_nd(
                    (batchBeamSeq + tf.cast(batch_size,tf.int64)*beam_path[k]),tmpind[-1])]
            actions = tmplst
        else: 
            actions = actions_tmp

        # put the depot in the first position
        # this makes it so that leaving the depot is taken into account in the reward function
        # also copy the depot beam_width many times
        depot = input_pnt[:,-1]
        depot = tf.tile(depot, multiples=[beam_width, 1])
        actions = [depot] + actions

        R, R_unsummed = self.reward_func(actions, final_state.tour_length, final_state.cur_machine)

        ### critic
        v = tf.constant(0)
        if decode_type=='stochastic':
            with tf.variable_scope("Critic"):
                with tf.variable_scope("Encoder"):
                    # init states
                    initial_state = tf.zeros([args['rnn_layers'], 2, batch_size, args['hidden_dim']])
                    l = tf.unstack(initial_state, axis=0)
                    rnn_tuple_state = tuple([tf.nn.rnn_cell.LSTMStateTuple(l[idx][0],l[idx][1])
                              for idx in range(args['rnn_layers'])])

                    hy = rnn_tuple_state[0][1]

                with tf.variable_scope("Process"):
                    for i in range(args['n_process_blocks']):

                        process = self.clAttentionCritic(args['hidden_dim'],_name="P"+str(i))
                        e,logit = process(hy, encoder_emb_inp, env)

                        prob = tf.nn.softmax(logit)
                        # hy : [batch_size x 1 x sourceL] * [batch_size  x sourceL x hidden_dim]  ->
                        #[batch_size x h_dim ]
                        hy = tf.squeeze(tf.matmul(tf.expand_dims(prob,1), e ) ,1)

                with tf.variable_scope("Linear"):
                    v = tf.squeeze(tf.layers.dense(tf.layers.dense(hy,args['hidden_dim']\
                                                               ,tf.nn.relu,name='L1'),1,name='L2'),1)


        return (R, v, logprobs, actions, idxs, env.input_pnt , probs, final_state.tour_length, final_state.cur_machine, R_unsummed)
    
    def build_train_step(self):
        '''
        This function returns a train_step op, in which by running it we proceed one training step.
        '''
        args = self.args
        
        R, v, logprobs, actions, idxs , batch , probs, _ , _, _ = self.train_summary

        v_nograd = tf.stop_gradient(v)
        R = tf.stop_gradient(R)

        # losses
        actor_loss = tf.reduce_mean(tf.multiply((R-v_nograd),tf.add_n(logprobs)),0)
        actor_loss_wo_bias = tf.reduce_mean(tf.multiply((R),tf.add_n(logprobs)),0)

        critic_loss = tf.losses.mean_squared_error(R,v)

        # optimizers
        actor_optim = tf.train.AdamOptimizer(args['actor_net_lr'])
        critic_optim = tf.train.AdamOptimizer(args['critic_net_lr'])

        # compute gradients
        actor_gra_and_var = actor_optim.compute_gradients(actor_loss,\
                                tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='Actor'))
        critic_gra_and_var = critic_optim.compute_gradients(critic_loss,\
                                tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='Critic'))

        # clip gradients
        clip_actor_gra_and_var = [(tf.clip_by_norm(grad, args['max_grad_norm']), var) \
                                  for grad, var in actor_gra_and_var]

        clip_critic_gra_and_var = [(tf.clip_by_norm(grad, args['max_grad_norm']), var) \
                                  for grad, var in critic_gra_and_var]

        # apply gradients
        actor_train_step = actor_optim.apply_gradients(clip_actor_gra_and_var)
        critic_train_step = critic_optim.apply_gradients(clip_critic_gra_and_var)

        train_step = [actor_train_step, 
                      critic_train_step ,
                      actor_loss,
                      critic_loss,
                      actor_gra_and_var,
                      critic_gra_and_var,
                      R,
                      v,
                      logprobs,
                      probs,
                      actions,
                      idxs,
                      actor_loss_wo_bias]
        return train_step

    def Initialize(self,sess):
        self.sess = sess
        self.sess.run(tf.global_variables_initializer())
        self.load_model()

    def load_model(self):
        if self.args["load_path"] != "":
            assert tf.train.checkpoint_exists(self.args["load_path"]), "Checkpoint {} does not exist!".format(self.args["load_path"])
            print("###############################################################")
            print("Loading checkpoint from {}".format(self.args["load_path"]))
            self.saver.restore(self.sess, self.args["load_path"])

    def evaluate_single(self, eval_type='greedy', eval_on_test=True, log_to_file=None):
        start_time = time.time()
        avg_reward = []
        print("Single evaluation with type {} on {} set.".
              format(eval_type,
              "test" if eval_on_test else "validation"))

        if eval_type == 'greedy':
            summary = self.val_summary_greedy
        elif eval_type == 'beam_search':
            summary = self.val_summary_beam
        self.dataGen.reset()

        print("Running a single evaluation")
        if log_to_file is not None:
            file_writer = open(log_to_file + '.txt', "w")

        iterator = self.dataGen.get_test_samples() if eval_on_test else self.dataGen.get_eval_samples()
        for step, (data, (gurobi_tour, gurobi_score)) in enumerate(iterator):
        #for step in range(self.dataGen.n_problems):
            #data, _ = self.dataGen.get_test_next()
            R, v, logprobs, actions,idxs, batch, _, tour_length, final_machine_idx, R_unsummed = self.sess.run(summary,
                                         feed_dict={self.env.input_data:data,
                                                   self.decodeStep.dropout:0.0})
            if eval_type=='greedy':
                avg_reward.append(R)
                R_ind0 = 0
                R_val = R
            elif eval_type=='beam_search':
                # R : [batch_size x beam_width]
                R = np.concatenate(np.split(np.expand_dims(R,1) ,self.args['beam_width'], axis=0),1 )
                R_val = np.amin(R,1, keepdims = False)
                R_ind0 = np.argmin(R,1)[0]
                avg_reward.append(R_val)

            if log_to_file is not None or step % int(self.args['log_interval']) == 0:
                beam_plus_batch_size = np.shape(batch)[0]
                example_output = []
                example_input = []
                for i in range(self.env.n_nodes):
                    example_input.append(list(batch[0, i, :]))
                for idx, action in enumerate(actions):
                    example_output.append(list(action[R_ind0*np.shape(batch)[0]]))

                if log_to_file is not None:
                    file_writer.write("Results for instance {}:\n".format(step))
                    file_writer.write("  Reward: {}\n".format(R_val))
                    reward_unsummed = R_unsummed[:,R_ind0]
                    file_writer.write("  Reward (unsummed): {}\n".format("->".join([str(x)
                                                                                    for x in reward_unsummed])))

                    file_writer.write("  Total log-probs (w/o tour length): {}\n".format(np.sum(logprobs[R_ind0])))

                    # find the permutation tour by trying to match the output action vectors
                    # with the input feature vectors
                    input_sequence = batch[0, :, :]
                    permutation_tour = []
                    for i, action_vectors in enumerate(actions):
                        idxs_to_add = []

                        # beam search returns all actions:
                        # take the one corresponding to the best hypothesis
                        action_vector = action_vectors[R_ind0 * beam_plus_batch_size]

                        # add all input vectors that match the action vector
                        # these can be multiples, if all the inputs are the same
                        for j in range(input_sequence.shape[0]):
                            input_vector = input_sequence[j, :]
                            if np.allclose(action_vector, input_vector):
                                idxs_to_add.append(j)

                        permutation_tour.append(idxs_to_add)
                    assert len(permutation_tour) == len(actions)

                    file_writer.write("  Tour: {}\n".format("->".join([str(x) for x in permutation_tour])))
                    file_writer.write("  Tour length (not including final return to depot): {}\n".format(tour_length[R_ind0]))

                    depot_idx = self.env.n_nodes - 1
                    file_writer.write("  # times depot (idx={}) was visited: {}\n".format(depot_idx,
                                                                                          sum([1 for idxs in permutation_tour
                                                                                               if depot_idx in idxs])
                                                                                          )
                                      )
                    file_writer.write("  Reached machine index: {}\n".format(final_machine_idx[R_ind0]))

                    if gurobi_score is not None:
                        file_writer.write("  Gurobi score: {}\n".format(gurobi_score))
                    if gurobi_tour is not None:
                        file_writer.write("  Gurobi tour: {}\n".format("->".join([str(x-1) for x in gurobi_tour])))

                    file_writer.write("  Features: {}\n".format(input_sequence))
                    file_writer.write("\n")

                if step % int(self.args['log_interval']) == 0:
                    self.prt.print_out('\n\nVal-Step of {}: {}'.format(eval_type,step))
                    self.prt.print_out('\nExample test input: {}'.format(example_input))
                    self.prt.print_out('\nExample test output: {}'.format(example_output))
                    self.prt.print_out('\nExample test reward: {} - best: {}'.format(R[0],R_ind0))
        end_time = time.time() - start_time

        if file_writer is not None:
            file_writer.write('\nValidation overall avg_reward: {}\n'.format(np.mean(avg_reward)))
            file_writer.write('Validation overall reward std: {}\n'.format(np.sqrt(np.var(avg_reward))))

            file_writer.write("Finished evaluation with %d steps in %s." % (step, time.strftime("%H:%M:%S",
                                                                                                 time.gmtime(
                                                                                                     end_time))))
            file_writer.close()

        # Finished going through the iterator dataset.
        self.prt.print_out('\nValidation overall avg_reward: {}'.format(np.mean(avg_reward)) )
        self.prt.print_out('Validation overall reward std: {}'.format(np.sqrt(np.var(avg_reward))) )

        self.prt.print_out("Finished evaluation with %d steps in %s." % (step\
                           ,time.strftime("%H:%M:%S", time.gmtime(end_time))))

        
    def evaluate_batch(self,eval_type='greedy', eval_on_test=True):
        print("Batched evaluation with type {} on {} set.".
              format(eval_type,
                     "test" if eval_on_test else "validation"))
        self.env.reset()
        if eval_type == 'greedy':
            summary = self.val_summary_greedy
            beam_width = 1
        elif eval_type == 'beam_search':
            summary = self.val_summary_beam
            beam_width = self.args['beam_width']

        data = self.dataGen.test_iterator.X if eval_on_test else self.dataGen.eval_iterator.X
        start_time = time.time()
        R, v, logprobs, actions,idxs, batch, _, _, _, _ = self.sess.run(summary,
                                     feed_dict={self.env.input_data:data,
                                               self.decodeStep.dropout:0.0})
        R = np.concatenate(np.split(np.expand_dims(R,1) ,beam_width, axis=0),1 )
        R = np.amin(R,1, keepdims = False)

        end_time = time.time() - start_time
        self.prt.print_out('Average of {} in batch-mode: {} -- std {} -- time {} s'.format(eval_type,\
            np.mean(R),np.sqrt(np.var(R)),end_time))        
        
    def inference(self, infer_type='batch', output_file=None):
        if infer_type == 'batch':
            self.evaluate_batch('greedy', eval_on_test=True)
            self.evaluate_batch('greedy', eval_on_test=False)

            self.evaluate_batch('beam_search', eval_on_test=True)
            self.evaluate_batch('beam_search', eval_on_test=False)
        elif infer_type == 'single':
            self.evaluate_single('greedy', eval_on_test=True, log_to_file=output_file + ".greedy.test" if output_file is not None else None)
            self.evaluate_single('beam_search', eval_on_test=True, log_to_file=output_file + ".beam_search.test" if output_file is not None else None)

            self.evaluate_single('greedy',
                                 log_to_file=output_file + ".greedy.validation" if output_file is not None else None,
                                 eval_on_test=False)
            self.evaluate_single('beam_search',
                                 log_to_file=output_file + ".beam_search.validation" if output_file is not None else None,
                                 eval_on_test=False)

        self.prt.print_out("##################################################################")

    def run_train_step(self):
        #data = self.dataGen.get_train_next()
        data, _ = next(self.dataGen.get_next_train_batch())

        train_results = self.sess.run(self.train_step,
                                 feed_dict={self.env.input_data:data,
                                  self.decodeStep.dropout:self.args['dropout']})
        return train_results
